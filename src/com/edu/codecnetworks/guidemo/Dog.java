package com.edu.codecnetworks.guidemo;
public class Dog {
	private int size;
	private int teeth;
	private String name;
	void bite(String x)
	{ 
	 System.out.println(x+" cries Ahhhh!");
	}
	void bark(int times)
	{ 
		int i;
		for(i=0;i<times;i++)
		{
			if(size>=70&&  size<=80)
			{
			 System.out.println("WOOF");	
			}
			else if(size>=40&& size<=60)
			{
				System.out.println("Bow");
			}
			else
			{
				System.out.println("yelh");
			}
		}
	
	}
    public void setSize(int i)throws Exception
    {
    	if (i>=20 && i<100)
    	{
    		size=i;
    	}
    	else
    	{
    		throw new Exception("Error : dog size should be 20 and 100,provided size is "+i);
    		
    	}
    }
    public byte getSize()
    {
    	return size;
    }
    public void setTeeth(int vl)throws Exception
    {
    	if(vl>0 && vl<=30)
    	{
    		teeth=vl;
    	}
    	else
    	{
    		throw new Exception("Error : dog teeth should in between 0 and 31,provided teeth are "+vl);
    	}
    }
    public int getTeeth()
    {
      return teeth;
    }
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
    
}
