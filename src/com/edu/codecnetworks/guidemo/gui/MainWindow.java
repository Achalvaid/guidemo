/**
 * 
 */
package com.edu.codecnetworks.guidemo.gui;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

import com.edu.codecnetworks.guidemo.Dog;

/**
 * @author Achal
 *
 */
public class MainWindow {
	JFrame frame = new JFrame("Example");
	JPanel[] p = new JPanel[4];
	JLabel[] label = new JLabel[3];
	JTextField[] textfeild = new JTextField[3];
	JButton submit=new JButton("ok");
	Dog doggy=new Dog(); 

	public MainWindow() {
		submit.addActionListener(new ActionListener(){

			@Override
			public void actionPerformed(ActionEvent arg0) {
				doggy.setName(textfeild[0].getText());
				try {
					doggy.setSize(Integer.parseInt(textfeild[1].getText()));
				} catch (Exception e) {
					textfeild[1].setForeground(Color.RED);
					textfeild[1].setText(e.getMessage());
					// TODO Auto-generated catch block
					
				}
				// TODO Auto-generated method stub
				try {
					doggy.setTeeth(Integer.parseInt(textfeild[2].getText()));
				} catch (Exception e) {
					textfeild[2].setForeground(Color.RED);
					textfeild[2].setText(e.getMessage());
					// TODO Auto-generated catch block
					
				}
				System.out.println("great");
				
			}
			
		});
		for (int i = 0; i < label.length; ++i) {
			label[i] = new JLabel();
			textfeild[i] = new JTextField(12);
		}
		label[0].setText("enter dog name");
		label[1].setText("dog height");
		label[2].setText("no of teeth");
		for (int i = 0; i < p.length; ++i) {
			p[i]= new JPanel();
			p[i].setLayout(new BoxLayout(p[i],BoxLayout.Y_AXIS));
		}
		for(int i=0;i<label.length;++i){
			p[i].add(label[i]);
			p[i].add(textfeild[i]);
		}
		p[3].setLayout(new BoxLayout(p[3],BoxLayout.Y_AXIS));
		p[3].add(p[0]);
		p[3].add(p[1]);
		p[3].add(p[2]);
		p[3].add(submit);
		frame.getContentPane().add(p[3]);
		frame.setVisible(true);
		frame.setSize(300, 300);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	}

	// @Override
	// public void actionPerformed(ActionEvent e) {
	// if(e.getSource()==button)
	// tton.setText("you clicked a button");
	// else if(e.getSource()==button1)
	// button1.setText("youclicked");
	// System.out.println("you clicked button");
	// TODO Auto-generated method stub

	// }
}